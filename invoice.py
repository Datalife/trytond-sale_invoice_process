# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.modules.sale.invoice import process_sale

__all__ = ['Invoice']


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._buttons.update({
            'process': {
                'invisible': (~Eval('state').in_(['draft', 'validated']) |
                    (Eval('type') != 'out')),
                'icon': 'tryton-launch',
                'depends': ['state', 'type']
            }
        })

    @classmethod
    @process_sale
    @ModelView.button
    def process(cls, records):
        pass
